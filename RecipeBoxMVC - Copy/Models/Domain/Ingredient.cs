﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain
{
    [Serializable]
    public class Ingredient
    {
        private string name;
        private List<Nutrient> nutrients = new List<Nutrient>();
        private Measurement measurement;

        public Ingredient() { }

        public void setName(string name) { this.name = name; }
        public string getName() { return name; }
        public List<Nutrient> getNurtients() { return nutrients; }
        public void addNutrient(Nutrient nut) { nutrients.Add(nut); }
        public void setMeasurement(Measurement mes) { this.measurement = mes; }
        public Measurement getMeasurement() { return measurement; }

        public Measurement convMeasurement(string conversion)
        { //not yet implemented
            return measurement;
        }
    }
}
