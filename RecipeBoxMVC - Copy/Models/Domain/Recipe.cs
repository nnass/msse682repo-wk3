﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;

namespace Domain
{
    [Serializable]
    public class Recipe
    {
        public int ID { get; set; }
        public string name { get; set; }
        public int baseServings { get; set; }
        public int servings { get; set; }
        public string method { get; set; }
        public double timeInMinutes { get; set; }
        public List<Ingredient> ingredients = new List<Ingredient>();
        public List<FoodCategory> categoryTags = new List<FoodCategory>(); 

        public Recipe() { }

        public void setName(string name) { this.name = name; }
        public string getName() { return name; }
        public int getBaseServings() { return baseServings; }
        public void setBaseServings(int servings) { this.baseServings = servings; }
        public int multServings(int num)
        {
            servings = num * baseServings;
            return servings;
        }
        public string getMethod() { return method; }
        public void setMethod(string method) { this.method = method; }
        public void setTime(double time) { timeInMinutes = time; }
        public double getTime() { return timeInMinutes; }
        public void addIngredient(Ingredient ing) { ingredients.Add(ing); }
        public List<Ingredient> getIngredients() {  return ingredients; }
        public int getNumIngred() { return ingredients.Count; }
        public void addCategory(FoodCategory cat) { categoryTags.Add(cat); }
        public List<FoodCategory> getCategories() { return categoryTags; }

        public bool validate()
        {
            if (name == null) return false;
            if (baseServings == 0) return false;
            if (method == null) return false;
            if (timeInMinutes == 0) return false;
            if (ingredients == null) return false;
            if (categoryTags == null) return false;
            else return true;
        }


        public override string ToString()
        {
            string ingredientList = "";

            foreach (Ingredient i in ingredients)
            {
                ingredientList = Convert.ToString(i.getMeasurement().getAmount()) + " " +
                                 i.getMeasurement().getType() + " " + i.getName() + "\n" + ingredientList;
            }
            return name + "      " + " Time:  " + Convert.ToString(getTime()) + " Minutes" + "\n \n" +
                   "Servings:  " + baseServings + " \n \n" +
                   "Ingredients:  " + ingredientList + "\n \n" +
                   "Directions:  " + getMethod();
                   
                    
        }
    }

    public class RecipeDBContext : DbContext
    {
        public DbSet<Recipe> Recipes { get; set; }
    }
}
