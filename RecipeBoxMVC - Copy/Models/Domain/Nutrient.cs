﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain
{
    [Serializable]
    public class Nutrient
    {
        private string name;
        private double per100Grams;

        public Nutrient() { }

        public void setName(string name) { this.name = name; }
        public string getName() { return name; }
        public void setConv(double conv) { this.per100Grams = conv; }
        public double getConv() { return per100Grams; }
    }
}
