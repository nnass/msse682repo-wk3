﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain
{
    [Serializable]
    public class Measurement
    {
        private string type;
        private double per100Grams;
        private double amount;

        public Measurement() { }

        public void setType(string type) { this.type = type; }
        public string getType() { return type; }
        public void setConv(double conv) { this.per100Grams = conv; }
        public double getConv() { return per100Grams; }
        public void setAmount(double amt) { this.amount = amt; }
        public double getAmount() { return amount; }
    }
}
