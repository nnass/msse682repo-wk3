﻿using System;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using Domain;

namespace Service
{
    public class RecipeLibSvc : IRecipeLibSvc
    {
        public void StoreRecipeLib(RecipeLibrary recipeLib)
        {
            try
            {
                FileStream fileStream = new FileStream("RecipeLib.bin", FileMode.Create, FileAccess.Write);
                IFormatter formatter = new BinaryFormatter();
                formatter.Serialize(fileStream, recipeLib);
                fileStream.Close();
            }
            catch (IOException ioException)
            {
                Console.WriteLine(ioException.ToString());
            }
        }
        public RecipeLibrary GetRecipeLib()
        {
            FileStream fileStream = new FileStream("RecipeLib.bin", FileMode.OpenOrCreate, FileAccess.Read);
            IFormatter formatter = new BinaryFormatter();
            RecipeLibrary recipeLib = new RecipeLibrary();
            try
            {
                recipeLib = formatter.Deserialize(fileStream) as RecipeLibrary;
                
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            fileStream.Close();

            return recipeLib;
        }
        public void UpdateRecipeLib(RecipeLibrary recipeLib)
        { 
        }
        public void DeleteRecipeLib(RecipeLibrary recipeLib)
        { 
        }

    }
}
