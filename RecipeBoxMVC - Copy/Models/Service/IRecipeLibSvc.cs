﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Domain;

namespace Service
{
    public interface IRecipeLibSvc : IService
    {
        void StoreRecipeLib(RecipeLibrary recipeLib);
        RecipeLibrary GetRecipeLib();
        void UpdateRecipeLib(RecipeLibrary recipeLib);
        void DeleteRecipeLib(RecipeLibrary recipeLib);
    }
}
