﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using Ninject;
using System.Linq;
using Domain;
using Moq;
using System.Collections.Generic;

namespace RecipeBoxMVC.Infrastructure {

public class NinjectControllerFactory : DefaultControllerFactory {
        private IKernel ninjectKernel;

        public NinjectControllerFactory() {
            ninjectKernel = new StandardKernel();
            AddBindings();
        }

        protected override IController GetControllerInstance(RequestContext requestContext,
            Type controllerType) {

            return controllerType == null
                ? null
                : (IController)ninjectKernel.Get(controllerType);
        }

        private void AddBindings() {


                ninjectKernel.Bind<IRecipeRepository>().To<EFRecipeRepository>();
            }
        }
}
