﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Service;

namespace Business
{
    public abstract class Manager
    {
        private Factory factory = Factory.GetInstance();

        protected IService GetService(string name)
        {
            return factory.GetService(name);
        }

    }
}
