﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain
{
    [Serializable]
    public class Measurement
    {
        public int ID { get; set; }
        private string type;
        private double per100Grams;
        public double amount { get; set; }

        public Measurement() { }

        public void setType(string type) { this.type = type; }
        public string getType() { return type; }
        public void setConv(double conv) { this.per100Grams = conv; }
        public double getConv() { return per100Grams; }
        public void setAmount(double amt) { this.amount = amt; }
        public double getAmount() { return amount; }
    }
}
