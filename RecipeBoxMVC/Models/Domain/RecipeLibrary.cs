﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain
{
    [Serializable]
    public class RecipeLibrary
    {
        private int size;
        private List<Recipe> recipes = new List<Recipe>();

        public RecipeLibrary() { }

        public bool addRecipe(Recipe recipe)
        {
            recipes.Add(recipe);
            size = recipes.Count;
            return true;
        }
        public int getSize() { return size; }
        
        public List<string> ListRecipes()
        {
            List<string> recipeName = new List<string>();
            foreach (Recipe r in recipes)
            {
                recipeName.Add(r.name);
            }
            return recipeName;
        }
        public Recipe getRecipe(int recNum)
        {
            return recipes[recNum];
        }


    }
}
