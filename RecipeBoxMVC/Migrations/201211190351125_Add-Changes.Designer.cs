// <auto-generated />
namespace RecipeBoxMVC.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class AddChanges : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddChanges));
        
        string IMigrationMetadata.Id
        {
            get { return "201211190351125_Add-Changes"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
