namespace RecipeBoxMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddChanges : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Recipes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        name = c.String(),
                        baseServings = c.Int(nullable: false),
                        servings = c.Int(nullable: false),
                        method = c.String(),
                        timeInMinutes = c.Double(nullable: false),
                        ownerID = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Ingredients",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        name = c.String(),
                        measurement_ID = c.Int(),
                        Recipe_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Measurements", t => t.measurement_ID)
                .ForeignKey("dbo.Recipes", t => t.Recipe_ID)
                .Index(t => t.measurement_ID)
                .Index(t => t.Recipe_ID);
            
            CreateTable(
                "dbo.Measurements",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        amount = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.FoodCategories",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        name = c.String(),
                        Recipe_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Recipes", t => t.Recipe_ID)
                .Index(t => t.Recipe_ID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.FoodCategories", new[] { "Recipe_ID" });
            DropIndex("dbo.Ingredients", new[] { "Recipe_ID" });
            DropIndex("dbo.Ingredients", new[] { "measurement_ID" });
            DropForeignKey("dbo.FoodCategories", "Recipe_ID", "dbo.Recipes");
            DropForeignKey("dbo.Ingredients", "Recipe_ID", "dbo.Recipes");
            DropForeignKey("dbo.Ingredients", "measurement_ID", "dbo.Measurements");
            DropTable("dbo.FoodCategories");
            DropTable("dbo.Measurements");
            DropTable("dbo.Ingredients");
            DropTable("dbo.Recipes");
        }
    }
}
