﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain;


namespace RecipeBoxMVC.Controllers
{
    public class RecipesController : Controller
    {
        private IRecipeRepository repository;
        private RecipeDBContext db = new RecipeDBContext();

        public RecipesController(IRecipeRepository repoParam)
        {
            repository = repoParam;
        }
        //
        // GET: /Recipes/



        public ViewResult Index()
        {
           
            
            return View(repository.Recipes
                                  .Where(p => p.ownerID == User.Identity.Name));
        }

        //
        // GET: /Recipes/Details/5

        public ActionResult Details(int id = 0)
        {
            
            Recipe recipe = db.Recipes.Find(id);
            if (recipe == null)
            {
                return HttpNotFound();
            }
            return View(recipe);
        }

        //
        // GET: /Recipes/Create

        public ActionResult Create()
        {
            Recipe recipe = new Recipe();
            recipe.ingredients = new List<Ingredient>();
            recipe.categoryTags = new List<FoodCategory>();
            Ingredient i1 = new Ingredient();
            FoodCategory fc1 = new FoodCategory();
            recipe.ingredients.Add(i1);
            recipe.categoryTags.Add(fc1);
        
            return View(recipe);
        }

        //
        // POST: /Recipes/Create

        [HttpPost]
        public ActionResult Create(Recipe recipe)
        {
            

            if (ModelState.IsValid)
            {
           
                recipe.ownerID = User.Identity.Name;
                db.Recipes.Add(recipe);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(recipe);
        }

        //
        // GET: /Recipes/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Recipe recipe = db.Recipes.Find(id);
            if (recipe == null)
            {
                return HttpNotFound();
            }
            return View(recipe);
        }

        //
        // POST: /Recipes/Edit/5

        [HttpPost]
        public ActionResult Edit(Recipe recipe)
        {
            if (ModelState.IsValid)
            {
                db.Entry(recipe).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(recipe);
        }

        //
        // GET: /Recipes/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Recipe recipe = db.Recipes.Find(id);
            if (recipe == null)
            {
                return HttpNotFound();
            }
            return View(recipe);
        }

        //
        // POST: /Recipes/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Recipe recipe = db.Recipes.Find(id);
            db.Recipes.Remove(recipe);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult IngredientRow()
        {
            return PartialView("IngredientEditor");
        }

    }
}