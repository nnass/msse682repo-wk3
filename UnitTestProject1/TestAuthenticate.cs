﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Ninject;
using RecipeBoxMVC.Models;
using System.Linq;
using System.Collections.Generic;
using RecipeBoxMVC.Controllers;
using System.Web.Mvc;
using System.Web;

namespace UnitTestProject1
{
    [TestClass]
    public class TestAuthenticate
    {
        [TestMethod]
        public void TestWCFAuthenticate()
        {
            //if User is correct
            LoginModel lm = new LoginModel();
            lm.Password = "nn817067";
            lm.UserName = "Nathan";

            AccountController controller = new AccountController();
            var controllerContext = new Mock<ControllerContext>();

            //and Login is called with localhost as url
            var result = controller.Login(lm, "localhost");

            Assert.IsTrue(result.ToString() == "localhost"); 

        }
    }
}
